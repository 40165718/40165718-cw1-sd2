﻿/* 40165718
 * Alexander Mowbray
 * Coursework 1
 * 22/10/2015
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 


    /* Creating the class for the students
     * This will allow me to  store multiple different objects all with the same attributes
     * this is useful because it will let me have more than one student saved at one time
     * which is probably useful.*/

    public partial class MainWindow : Window
    {
        private student Student;
        public MainWindow()
        {
            InitializeComponent();
            Student = new student();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            /* The clear button wipes all the data 
             * from the text boxes on the form
             * But not from the Class*/
            FirstName.Text = String.Empty;
            LastName.Text = String.Empty;
            DOB.Text = String.Empty;
            Course.Text = String.Empty;
            MatricNum.Text = String.Empty;
            Level.Text = String.Empty;
            Credits.Text = String.Empty;
        }

        private void Get_Click(object sender, RoutedEventArgs e)
        {
            /* The Get button is used for setting the text boxes on the form to most recent 
             * info that has been stored in the class. */
            FirstName.Text = Student.FirstName;
            LastName.Text = Student.LastName;
            DOB.Text = Student.DoB.ToShortDateString();
            Course.Text = Student.Course;
            MatricNum.Text = Student.Matricnum.ToString();
            // Have to convert the int values of these to back to stirngs
            Level.Text = Convert.ToString(Student.Level);
            Credits.Text = Convert.ToString(Student.Credit);
        }

        private void Set_Click(object sender, RoutedEventArgs e)
        {

            //The Set button is used to add the current values of the textboxes into the student class

           int tempmatNum = 0; //temp data to dfill in blanks if they dont work
            if (!int.TryParse(MatricNum.Text, out tempmatNum)) //Tries to poarse the dayta if not then it cant work.
            {
                MessageBox.Show("You have not entered a valid Matriculation Number");
            }
            DateTime tempDOB = DateTime.Parse("01/01/0001");
            if (!DateTime.TryParse(DOB.Text, out tempDOB))
            {
                MessageBox.Show("You have not entered a valid Date of Birth");
            }
            int templevel = 0;
            if (!int.TryParse(Level.Text, out templevel))
            {
                MessageBox.Show("you have not entered A valid level");
            }
            int tempcredit = 0;
            if (!int.TryParse(Credits.Text, out tempcredit))
            {
                MessageBox.Show("you have not entered a valid amount of credits");
            }
            
            //Setting all my attritbuteds to the temp files that should be correct
            Student.Credit = tempcredit;
            Student.Level = templevel;
            Student.DoB = tempDOB;
            Student.Matricnum = tempmatNum;
            Student.FirstName = FirstName.Text;
            Student.LastName = LastName.Text;
            Student.Course = Course.Text;
        }

        private void Advance_Click(object sender, RoutedEventArgs e)
        {
            // goes to the advance method and then edits the text box.
            Student.Advance();
            Level.Text = Convert.ToString(Student.Level);
        }

        private void Award_Click(object sender, RoutedEventArgs e)
        {
            Student.award();// Runs the method to see what level of awars the student got
            AwardForm Awards = new AwardForm();
            Awards.Show();// Shows the Award form
            //Prints all the values into the form
            Awards.MatricNum.Text = Convert.ToString(Student.Matricnum);
            Awards.LastName.Text = Student.LastName;
            Awards.FirstName.Text = Student.FirstName;
            Awards.Course.Text = Student.Course;
            Awards.Level.Text = Convert.ToString(Student.Level);
            Awards.Award.Text = Student.Awards;
        }
    }
}
