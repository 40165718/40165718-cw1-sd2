﻿/* 40165718
 * Alexander Mowbray
 * Coursework 1
 * 22/10/2015
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework
{
    class student
    {
        //Declare the attributes
        private string firstname;
        private string lastname;
        // DOB stands for date of birth
        private DateTime DOB;
        private string course;
        private int matricnum;
        private int level;
        private int credits;
        private string awards;


        //All the gets and sets for the values
        public string FirstName
        {
            get { return firstname; }
            set {
                if(value != string.Empty) // checks to see if the string is empty.
                {
                    firstname = value; // if not then the value is saved
                } 
                else
                {
                    MessageBox.Show("You have not entered A first Name."); // if so then an error is displayed
                }
           }
        }

        public string LastName
        {
            get { return lastname; }
            set {
                if (value != string.Empty) //checking that the text box is not empty
                {
                    lastname = value; // if not then it saves the value
                }
                else
                {
                    MessageBox.Show("You have not entered a last Name."); // if it is it displays an error
                }
                }
        }

        public DateTime DoB
        {
            get { return DOB;} 
            set {
                if (value > DateTime.Today) // Checks to see if the date you entered is after today
                {
                    MessageBox.Show("You can not be born after today."); // if it is it dispalys an error
                }
                else if (value > DateTime.Today.AddYears(-17)) // checks to see you are not to youung to be at uni
                {
                    MessageBox.Show("You are not old enough to be at university"); // if you are there is an error
                }
                else 
                {
                    DOB = value; // if the vlaue is valid then it is saved
                }
            }
        }

        public string Course
        {
            get { return course;}
            set 
            {
                if (value != string.Empty) // checks to make sure the string isnt empty
                {
                    course = value; //if it is not empty then the value is saved
                }
                else
                {
                    MessageBox.Show("You have not entered a Course Name"); // if it is empty an error message is displayed
                }
            }
        }

        public int Level
        {
            get { return level; }
            set 
            {
                if (value.ToString() == string.Empty) // checks to see if the text box is empty
                {
                    MessageBox.Show("you need to enter A value for the level"); // if it is there is an error message
                }
                else if(value <1 || value >4 ) // checks to see if your level is with in the limits
                {
                    MessageBox.Show("The level you have entered is out of the limits."); // if it isnt an error message is displayed
                }
                else
                {
                    level = value; // if all the data is valid the value is stored
                }
            }
        }

        public int Matricnum
        {
            get { return matricnum;}
            set 
            {
                if (value.ToString() != string.Empty) // checks to see if the text boxes is empty
                {
                    matricnum = value; // if not the the value is saved
                }
                else
                {
                    MessageBox.Show("you need to enter a value for your matriculation number"); // if the string is empty then an error is displayed
                }
            }
        }

        public int Credit
        {
            get { return credits; }
            set 
            {
                if (value.ToString() == string.Empty) // checks to see if the sting is empty 
                {
                    MessageBox.Show("You need to enter the number of credits you have."); // if it is then an error is displayed
                }
                else if (value < 0 || value > 480) // checks that the value is within the limits
                {
                    MessageBox.Show("The Value you have entered for credits is invalid please enter a number between 0 and 480 "); // if not then an error is displayed
                }
                else
                {
                    credits = value; // if all the validation is passed then the data is saved
                }
            }
        }

        public string Awards
        {
            get { return awards; }
            set { awards = value; }
        }
        //Advance method is used for deciding whether or not the student is able to advance to the next level.
  
        public void Advance()
        {
            int number = 1;
            switch (number)
            {
                default: //the default level for all students is level 1 
                    level = 1;
                    break;
                case 1: //Checks to see if the student can proceed from level 1 to level 2
                    if (credits >= 120)
                    {
                        MessageBox.Show("Advancing");
                        level = 2;
                        break;
                    }
                    else
                    {
                        goto case 2;
                    }

                case 2: //checks to see if the student can proceed to level 3
                    if (credits >= 240)
                    {
                        MessageBox.Show("Advancing");
                        level = 3;
                        break;
                    }
                    else
                    {
                        goto case 3;
                    }

                case 3: // checks to see if the student can proceed to level 4
                    if (credits >= 360)
                    {
                        MessageBox.Show("Advancing");
                        level = 4;
                        break;
                    }
                    else
                    {
                        goto case 4;
                    }
                case 4: // if the student is already at level 4 it tells the m that they cant proceed
                    if (level == 4)
                    {
                        MessageBox.Show("Cannot advance past level 4");
                        break;
                    }
                    else
                    {
                        goto case 5;
                    }

                case 5: // if the student doesnt have enough levels for a proceed they are sent back to level 1.
                    if (credits < 120)
                    {
                        MessageBox.Show("You do not have enought Credits to advance.");
                        goto default;
                    }
                    break;
            }
        }

        public void award()// This is the method that allows you to 
        {
            int num = 1;
            switch (num)
            {
                default:// the defualt is that if you have less than the amount of credits required to pass you get a certificate.
                    awards = "Certificate of higher Education";
                    break;
                case 1://Case 1 says that if you got enough cresits to pass but not ewnough for honours you get a degree.
                    if (credits >= 360 || credits <= 479 )
                    {
                        awards = "Degree";
                        break;
                    }
                    else
                    {
                        goto case 2;
                    }
                case 2: // Case 3 says if you have have more than the amount of credits required for honours you get honours.
                    if (credits >= 480)
                    {
                        awards = "Honours Degree";
                        break;
                    }
                    else
                    {
                        goto default;
                    }
            }
        }
    }
}